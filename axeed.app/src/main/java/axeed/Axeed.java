/*
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-v20.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Copyright Contributors to the axeed Project.
 */
package axeed;

import java.io.IOException;
import java.net.URI;
import java.util.Set;

import javax.annotation.ManagedBean;
import javax.ws.rs.core.Application;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.jboss.weld.environment.se.Weld;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

import axeed.front.endpoints.ResourceEndPoint;
import axeed.front.endpoints.TaskEndPoint;
import axeed.front.utils.CorsFilter;
import axeed.front.utils.GsonMessageBodyHandler;

/**
 * Main Java application running the Axeed Server.
 * 
 * @author Goulwen LE FUR
 */
public class Axeed {
	
	private static final Logger logger = LoggerFactory.getLogger(Axeed.class);

	private static final String APP_NAME = "axeed";
	private static final String API_SEGMENT = "/api";
	
	private static String BASE_URL = "http://localhost";
	private static int PORT = 8080;

    public static void main(String[] args) throws ParseException {
        try {
        	logger.info("Starting " + APP_NAME + " application...");

        	final Options options = configureOptions();
            final CommandLineParser parser = new DefaultParser();
            final CommandLine line = parser.parse(options, args);   
            
            String baseURLParam = line.getOptionValue("u");
            if (baseURLParam != null) {
            	BASE_URL = baseURLParam;
            }
            String port = line.getOptionValue("p");
            if (port != null) {
            	try {
            		int parseInt = Integer.parseInt(port);
            		PORT = parseInt;
            	} catch (Exception e) {
            		logger.error("Invalid port value. Using default value instead (" + PORT + ")");
            	}
            }
            
            String SERVER_PATH = BASE_URL + ":" + PORT + "/" + APP_NAME;
			URI BASE_URI = URI.create(SERVER_PATH + API_SEGMENT + "/");
            
			final Weld weld = new Weld();
            weld.initialize();
            final ResourceConfig resourceConfig = ResourceConfig.forApplication(ResourceConfig.forApplicationClass(JaxRsApplication.class));
            final HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, resourceConfig, false);
            
            boolean backend = line.hasOption("b");
            if (!backend) {
	            ClassLoader classLoader = Axeed.class.getClassLoader();
	            server.getServerConfiguration().addHttpHandler(new CLStaticHttpHandler(classLoader, "/static/"), "/" + APP_NAME + "-static/");
	            server.getServerConfiguration().addHttpHandler(new CLStaticHttpHandler(classLoader, "/web/"), "/" + APP_NAME + "/");
            	logger.info("Front-end resources registered");
            } else {
            	logger.info("Running " + APP_NAME + " in back-end mode");
            }

            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    server.shutdownNow();
                    weld.shutdown();
                }
            }));
           
            server.start();

            logger.info(APP_NAME + " started! Ready to go buddy! You can access the application on " + SERVER_PATH  + "/");

            Thread.currentThread().join();
        } catch (InterruptedException | IOException ex) {
        	logger.error("An error occurred.", ex);
        }

    }


    
    /**
     * JAX-RS application defined as a CDI bean.
     */
    @ManagedBean
    public static class JaxRsApplication extends Application {
    	@Override
    	public Set<Class<?>> getClasses() {
    		return Sets.newHashSet(
    				ResourceEndPoint.class,
    				TaskEndPoint.class,
    				GsonMessageBodyHandler.class, 
    				CorsFilter.class);
    	}

    }
    
	protected static Options configureOptions() {
		final Option baseURLOption = Option.builder("u")
									.longOpt("base-url")
									.desc("Define the base URL for the application (default: http://localhost)")
									.hasArg(true)
									.argName("baseURL")
									.required(false)
										.build();
		
		final Option portOption = Option.builder("p")
									.longOpt("port")
									.desc("Define the port to use for the application (default: 8080)")
									.hasArg(true)
									.argName("port")
									.required(false)
										.build();
		final Option backendOption = Option.builder("b")
										.longOpt("back-end")
										.desc("This option defines whether the application must be run in 'back-end' mode (i.e. just serving the API, not front resources) or not.")
										.required(false)
											.build();
		
		final Options options = new Options();
		options.addOption(baseURLOption);
		options.addOption(portOption);
		options.addOption(backendOption);
		return options;
	}

    

}

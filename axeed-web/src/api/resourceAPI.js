import api from './api'

export default {

  async count() {
    const response = await api.get('/resource/count')
    return response.data
  }

}
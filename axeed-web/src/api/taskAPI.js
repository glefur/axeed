import api from './api'

export default {

  async all() {
    const response = await api.get('/task')
    return response.data
  }

}
# Cookbook to rename the axeed app by your very own super project name

1 - Rename the top level directory and its applicative subdirectories according to your project name

## Back end projects

First let's rename all backend projects... with Eclipse

2.1 - Import all the eclipse into your workspace. Notice: in the first step your change the directories names, not the projects name. Therefore, Eclipse will display the original names for the projects (axeed, axeed.app...), not names according your directories.
2.2 - Rename all projects.
2.3 - Fix groupId, artifactId and name in all pom.xml in the project. 
2.3.1 - Update modules paths in the parent pom.
2.3.2 - Update mainClass in the app project pom.
3 - Rename the root packages of each java project
4 - Rename the main class of the .app project (according to the name you gave in the 2.3.2 step of this guide)
4.1 - Update the  ```APP_NAME``` constant of this class

We're good for the back end. Let's workout the front end.

## Front end project

Let's work with... VSCode... why not. Launch VSCode in the XXX-web project

1 - Update the application ```name``` in the package.json
2 - Update the ```VUE_APP_API_URL``` values in the .env and .env.development files
3 - Finally, update the application ```publicPath``` in the vue.config.js file

## Build script update

Finally, just change the ```PROJECT_NAME``` in the build/build.sh script

Et voilà. You can run the app in dev mode, in production and build it with the dedicated script!.



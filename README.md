# axeed

Template project for Mongo/JEE/VueJS projects

## Build

At project's root 

```
mvn clean package
```

## Run

```
java -jar axeed.app/target/axeed.app-0.7.0-SNAPSHOT.jar
```




/*
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-v20.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Copyright Contributors to the axeed Project.
 */
package axeed.managers;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.PostConstruct;

import com.google.common.collect.Lists;

import axeed.business.Task;

/**
 * @author Goulwen LE FUR
 *
 */
public class TaskManager {
	
	private List<Task> tasks;

	public TaskManager() {
		tasks = Lists.newArrayList();
	}

	public List<Task> getTasks() {
		return tasks;
	}
	
	public Optional<Task> get(String id) {
		if (id == null) {
			return Optional.empty();
		}
		return tasks.stream().filter(t -> id.equals(t.getId())).findFirst();
	}
	
	@PostConstruct
	private void init() {
		
		Builder.newInstance(tasks)
			.title("Checkout Axeed code")
			.description("Go the the axeed repository and clone it to get the source code")
			.priority(1)
				.build();

		Builder.newInstance(tasks)
			.title("Rename axeed with your application name")
			.description("Run the -future- appropriate script. For the moment you have to rename it by hand ^^'.")
			.priority(2)
				.build();

		Builder.newInstance(tasks)
			.title("Extend the application with your own business")
			.description("Add views, models and services to create your application")
			.priority(1)
				.build();

		Builder.newInstance(tasks)
			.title("Run it!")
			.description("./build.sh, java -jar and lezgo!")
			.priority(1)
				.build();
	}
	
	private static final class Builder {

		private static final Builder newInstance(List<Task> tasks) {
			return new Builder(tasks);
		}
		
		private List<Task> tasks;
		private Task task;
		
		private Builder(List<Task> tasks) {
			this.tasks = tasks;
			task = new Task();
			task.setId(UUID.randomUUID().toString());		
		}
		
		public Builder title(String title) {
			this.task.setTitle(title);
			return this;
		}
		
		public Builder description(String description) {
			this.task.setDescription(description);
			return this;
		}
		
		public Builder priority(int priority) {
			this.task.setPriority(priority);
			return this;
		}
		
		public void build() {
			this.tasks.add(this.task);
		}
		
	}
	
}

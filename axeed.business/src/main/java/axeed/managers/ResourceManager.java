/*
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-v20.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Copyright Contributors to the axeed Project.
 */
package axeed.managers;

import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import com.google.common.collect.Lists;

import axeed.business.Resource;

/**
 * @author Goulwen LE FUR
 *
 */
public class ResourceManager {
	
	private List<Resource> resources;

	public ResourceManager() {
		resources = Lists.newArrayList();
	}

	public List<Resource> getResources() {
		return resources;
	}
	
	@PostConstruct
	private void init() {
		Resource sample = new Resource();
		sample.setId(UUID.randomUUID().toString());
		sample.setName("Sample Resource #1");
		resources.add(sample);
	}
	
}

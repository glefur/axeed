/*
 * This program and the accompanying materials are made available under the terms of the
 * Eclipse Public License v2.0 which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-v20.html
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Copyright Contributors to the axeed Project.
 */
package axeed.front.endpoints;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import axeed.managers.TaskManager;

/**
 * @author Goulwen LE FUR
 *
 */
@Path("/task")
public class TaskEndPoint {

	@Inject
	private TaskManager manager;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response all() {
		return Response
					.ok(manager.getTasks())
					.build();
	}
	
}

/*
This program and the accompanying materials are made available under the terms of the
Eclipse Public License v2.0 which accompanies this distribution, and is available at
https://www.eclipse.org/legal/epl-v20.html
SPDX-License-Identifier: EPL-2.0
Copyright Contributors to the axeed Project.
*/
function countResources() {
	var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
			   document.getElementById("count").innerHTML = xhttp.responseText;
		    }
		};
		xhttp.open("GET", "/axeed/api/resource/count", true);
		xhttp.send();
}

countResources()

#!/bin/bash

MVN_COMMAND=mvn
NPM_COMMAND=npm

# Define here the name of your project and the folder where the source projects are located

PROJECT_NAME=axeed
SRC_DIR=`pwd`/../

# You can change here 

PARENT_PROJECT=.
SERVER_PROJECT=$PROJECT_NAME.app
WEB_PROJECT=$PROJECT_NAME-web

BUILD_DIR=`pwd`

echo "OK, so build directory is defined as $BUILD_DIR. Going to $SRC_DIR"
cd $SRC_DIR

echo "Building front end..."
cd $WEB_PROJECT
$NPM_COMMAND run build

echo "Done. Next moving frontend build in server project..."
cd $SRC_DIR

if [[ -d "$SERVER_PROJECT/src/main/resources/web" ]]
then
  rm -rf "$SERVER_PROJECT/src/main/resources/web"
fi

mv "$WEB_PROJECT/dist" "$SERVER_PROJECT/src/main/resources/web"

echo "Done. Now building backend..."
$MVN_COMMAND clean package -f "$PARENT_PROJECT/pom.xml"

rm -rf "$SERVER_PROJECT/src/main/resources/web"

echo "Finally moving build result to build directory"
find "$SERVER_PROJECT/target" -maxdepth 1 -name "*.jar" -exec mv {} "$BUILD_DIR" \;
mv "$SERVER_PROJECT/target/lib" "$BUILD_DIR"

cd "$BUILD_DIR"

echo "Done. Everything is ok! Have fun with $PROJECT_NAME"
